# Deevotech Code Challenges

Hey! Thank you for your application. We appreciate your effort and time in participating this recruitment process. 
As English will be a part of the test, we encourage you to use it to answer your challenge (not compulsory if you are not comfortable with it). Now let’s take a look at your coding skills 😀.

Please read everything carefully and don’t hesitate to ask if you encounter any difficulties or have any questions regarding the evaluation. 
Select one of the team specific challenges and create a new git repository for that project.

## Duration of the challenge

We value your time and don't want you to spend more than 3 hours on your challenge (this challenge is scoped for about 1 hour timeframe). What we’d like to see are your thinking process and development patterns. If there are features that you don't have time to implement, feel free to use pseudo code to describe your ideas.

## Important Notes

- Your repository must have a README file which describe: the languages / framework of your choice and how to run / build your app
- The challenge is not only to solve the problem but showcase how much you know about good programming practices.
- If you have any questions, reach out to us and we are here to help, you are not going to be penalized by this (remember knowing how to ask is also an art itself 😉)

## What we review

- Correctness: Does the application perform what was asked? If there is anything missing, justify in the README file.
- Code Quality: Is the code simple, easy to understand? and maintainable?
- Technical Choices: Do choices of libraries, databases, architecture seem appropriate for the challenge?

## Get Ready

Fork this repository and start working on the challenge you select, then send us a link to your repository or create a pull request.

## Challenges

- [Fresher Javascript (React / VueJS)](/javascript-challenge.md)
